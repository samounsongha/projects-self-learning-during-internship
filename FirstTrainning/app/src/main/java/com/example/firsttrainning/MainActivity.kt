package com.example.firsttrainning

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         buttonRegister.setOnClickListener{
             val intent = Intent(this, LinearActivity::class.java)
             startActivity(intent)
         }
    }

}
