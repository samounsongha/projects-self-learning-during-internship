package com.example.listviewpractice

import android.content.Context
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listView = findViewById<ListView>(R.id.main_listview)

        listView.adapter = myCustomeAdapter(this) // this needs to be the custom adapter telling my list what to render
    }

    private class myCustomeAdapter(context: Context) : BaseAdapter() {

        private val myContext: Context

        init {
            myContext = context
        }

        //responsible for counting the row in the list
        override fun getCount(): Int {
            return 5
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }


        override fun getItem(position: Int): Any {
            return "Test String"
        }

        //responsible for rendering out each row
        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {

            val layoutInflater = LayoutInflater.from(myContext)
            val rowMain =  layoutInflater.inflate(R.layout.row_main, viewGroup, false)
            val positionTextVeiew = rowMain.findViewById<TextView>(R.id.position_textView)
            positionTextVeiew.text = "Row Number $position"
            return rowMain

//            val textView = TextView(myContext)

//            textView.text = "Here is my ROW for List View"
//            textView.setPadding(20,5,5,5)
//            return textView
        }
    }

}
