package com.example.recyclerviewbasic

data class User(val name: String, val address: String)