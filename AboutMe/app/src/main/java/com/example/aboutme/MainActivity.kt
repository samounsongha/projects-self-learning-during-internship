package com.example.aboutme

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.aboutme.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
            button_submit.setOnClickListener{
            val intent = Intent(this, LinearActivity::class.java)
            startActivity(intent)
        }

        binding.btnSubmit.setOnClickListener {
            addNickname(it)
        }
        binding.nicknameText.setOnClickListener() {
            updateNickname(it)
        }
    }

    private fun addNickname(view: View) {

        binding.nicknameText.text = binding.nicknameEdit.text.toString()
        binding.nicknameEdit.visibility = View.GONE
        binding.btnSubmit.visibility = View.GONE
        binding.nicknameText.visibility = View.VISIBLE

        //hide the keyboard
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun updateNickname(view: View) {
        val editText = findViewById<EditText>(R.id.nickname_edit)
        val doneButton = findViewById<Button>(R.id.btn_submit)
        editText.visibility = View.VISIBLE
        doneButton.visibility = View.VISIBLE
        view.visibility = View.GONE

        editText.requestFocus()

        //show the keyboard
        val showKeyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        showKeyboard.showSoftInput(editText,0)
    }

}
