package com.example.aboutme

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_linear.*
import kotlin.random.Random

class LinearActivity : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linear)

        val rollButton: Button = findViewById(R.id.roll_button)
        val countButton: Button = findViewById(R.id.countup_button)
        val resetButton: Button = findViewById(R.id.reset_value)

        dice_roller.setOnClickListener { goRoller() }
        back_home.setOnClickListener { backHome() }
        rollButton.setOnClickListener { rollDice() }
        countButton.setOnClickListener { countUp() }
        resetButton.setOnClickListener { resetValue() }

    }

    private fun rollDice() {
        val resultText: TextView = findViewById(R.id.text_value)
        val randomInt = Random.nextInt(6) + 1
        resultText.text = randomInt.toString()
        Toast.makeText(this, "You got " + randomInt.toString(),
            Toast.LENGTH_SHORT).show()
    }

    private fun countUp() {
        var resultText: TextView = findViewById(R.id.text_value)

        if (resultText.text == "Hello World!") {
            resultText.text = "1"
        }else {
            var resultInt = resultText.text.toString().toInt()

            if (resultInt < 6) {
                resultInt++
                resultText.text = resultInt.toString()
            }
        }
    }

    private fun resetValue() {
        var resultText: TextView = findViewById(R.id.text_value)
        var resultInt = resultText.text.toString().toInt()
        if (resultInt <= 6) {
            resultText.text = "0"
        }
    }

    private fun goRoller() {
        var intentGo = Intent(this, RollerActivity::class.java)
        startActivity(intentGo)
    }

    private fun backHome() {
        var intentBack = Intent(this, MainActivity::class.java)
        startActivity(intentBack)
    }
}