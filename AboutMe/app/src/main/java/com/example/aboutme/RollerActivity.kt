package com.example.aboutme

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_roller.*
import kotlin.random.Random

class RollerActivity : AppCompatActivity() {
    lateinit var diceImage : ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_roller)

        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener { diceRoller() }

        back_linear.setOnClickListener { backLinear() }
    }

    private fun diceRoller() {
        val randomInt = Random.nextInt(6) + 1
        val diceImage: ImageView = findViewById(R.id.dice_image)

        val drawableResource = when(randomInt) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        diceImage.setImageResource(drawableResource)

    }

    private fun backLinear() {
        val backLinear = Intent(this,LinearActivity::class.java)
        startActivity(backLinear)
    }
}