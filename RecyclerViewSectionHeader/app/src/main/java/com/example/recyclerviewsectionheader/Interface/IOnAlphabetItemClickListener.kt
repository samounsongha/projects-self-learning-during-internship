package com.example.recyclerviewsectionheader.Interface

import java.text.FieldPosition

interface IOnAlphabetItemClickListener {
    fun onAlphabetItemClick(value:String, position:Int)
}