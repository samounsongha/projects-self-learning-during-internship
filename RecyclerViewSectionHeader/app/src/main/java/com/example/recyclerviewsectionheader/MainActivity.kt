package com.example.recyclerviewsectionheader

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.recyclerviewsectionheader.Adapter.PersonAdapter
import com.example.recyclerviewsectionheader.Common.Common
import com.example.recyclerviewsectionheader.Common.LinearLayoutManagerWithSmootScroller
import com.example.recyclerviewsectionheader.Model.Person
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    internal var personList = ArrayList<Person>()
    internal lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        layoutManager = LinearLayoutManagerWithSmootScroller(this)

        recyclerview_person.layoutManager = layoutManager
        recyclerview_person.addItemDecoration(DividerItemDecoration(this,layoutManager.orientation))

        createPersonList()
        val adapter = PersonAdapter(this,personList)
        recyclerview_person.adapter = adapter

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Common.RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val result = data!!.getStringExtra("result")
                val position = Common.findPositionWithName(result,personList)
                recyclerview_person.smoothScrollToPosition(position)
            }
        }
    }


    private fun createPersonList() {
        personList = Common.genPersonGroup()
        personList = Common.sortList(personList)
        personList = Common.addAlphabet(personList)
    }
}
